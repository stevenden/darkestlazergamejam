﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelAnimationEnemyaiLarge : MonoBehaviour
{
    public enemyAI enemyai;
    public LargeEnemyAI largeEnemyAi;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void AttackPlayer()
    {
        largeEnemyAi.Lunge();
    }
    public void Rest()
    {
        largeEnemyAi.Rest();
    }
    public void StartLooking()
    {
        largeEnemyAi.StartLooking();
    }
    public void PlayFootSound()
    {
        this.GetComponent<AudioSource>().Play();
    }
}

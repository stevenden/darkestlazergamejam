﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class enemyAI : MonoBehaviour
{
    public NavMeshAgent navAgent;
    public GameObject player;
    public float attackDistance;
    public float followDistance;
    public int damage;
    public Animator animator;
    private bool attacking;
    public float enemyHealth;
    public ParticleSystem deathParticle;
    public bool enemyDead = false;
    public AudioSource attackAudio;
    //public AudioSource deathAudio;
    public AudioSource hurtAudio;

    // Start is called before the first frame update
    void Start()
    {
        navAgent = this.GetComponent<NavMeshAgent>();
        animator = this.GetComponentInChildren<Animator>();
    }

    //A function that is called from gun logic to hurt the enemy
    public void DamageEnemy(float damage)
    {
        enemyHealth -= damage;
    }

    // Update is called once per frame
    void Update()
    {
        if (enemyHealth <= 0 && !enemyDead)
        {
            enemyDead = true;
            //deathAudio.Play();
            this.GetComponent<enemyAI>().enabled = false;
            this.GetComponent<NavMeshAgent>().enabled = false;
            Instantiate(deathParticle, gameObject.transform.position, Quaternion.identity);
            Destroy(gameObject);
        }

        if (player != null && Vector3.Distance(player.transform.position, transform.position) < followDistance)
        {
            navAgent.destination = player.transform.position;
            if (!attacking)
            {
                if (Vector3.Distance(navAgent.destination, transform.position) < attackDistance)
                {
                    //attack
                    navAgent.speed = 0.1f;
                    animator.SetTrigger("attack");
                }
                else
                {
                    navAgent.speed = 3f;
                }
            }
        }

    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 9 || other.gameObject.CompareTag("Player"))
        {
            player = other.gameObject;
            navAgent.destination = other.transform.position;
            hurtAudio.Play();
            PlayerHealth.healthValue -= damage;
            Debug.Log("PLAYER!");
        }
    }


    private void OnTriggerExit(Collider other)
    {
        Debug.Log("trigggger");
        if (other.gameObject.layer == 9)
        {
            player = null;
            //navAgent.destination = other.transform.position;
        }
    }
    public void Lunge()
    {
        attacking = true;
        attackAudio.Play();
        navAgent.speed = 300f;
        navAgent.acceleration = 400f;
    }
    public void Rest()
    {
        //lunging = false;
        navAgent.isStopped = true;
        animator.SetTrigger("walk");
    }
    public void StartLooking()
    {
        attacking = false;
        navAgent.isStopped = false;
        navAgent.speed = 3f;
        navAgent.acceleration = 8f;
    }
}

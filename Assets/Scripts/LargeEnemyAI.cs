﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LargeEnemyAI : MonoBehaviour
{
    public NavMeshAgent navAgent;
    public GameObject player;
    public float attackDistance, followDistance;
    public float attackSpeed; //Rate at which enemy attacks
    public int damage;
    public Animator animator;
    private bool attacking;
    public float enemyHealth;
    public ParticleSystem deathParticle;
    public bool enemyDead = false;
    public AudioSource attackAudio;
    public AudioSource deathAudio;
    public AudioSource hurtAudio;

    // Start is called before the first frame update
    void Start()
    {
        navAgent = this.GetComponent<NavMeshAgent>();
        animator = this.GetComponentInChildren<Animator>();
    }

    //A function that is called from gun logic to hurt the enemy
    public void DamageEnemy(float damage)
    {
        enemyHealth -= damage;
    }

    // Update is called once per frame
    void Update()
    {
        if (enemyHealth <= 0 && !enemyDead)
        {
            enemyDead = true;
            deathAudio.Play();
            Instantiate(deathParticle, gameObject.transform.position, Quaternion.identity);
            Destroy(gameObject);
        }

        if (player != null && Vector3.Distance(player.transform.position, transform.position) < followDistance)
        {
            navAgent.destination = player.transform.position;
            if (!attacking)
            {
                if (Vector3.Distance(navAgent.destination, transform.position) < attackDistance)
                {
                    //attack
                    navAgent.speed = 0.1f;
                    animator.SetTrigger("attack");
                }
                else
                {
                    navAgent.speed = 3f;
                }
            }
        }

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 9 || other.gameObject.CompareTag("Player"))
        {
            player = other.gameObject;
            navAgent.destination = other.transform.position;
            StartCoroutine(EnemyAttack());
        }
    }


    public void Lunge()
    {
        attacking = true;
        attackAudio.Play();
        this.GetComponent<SphereCollider>().radius = 5f;
    }
    public void Rest()
    {
        //lunging = false;
        navAgent.isStopped = true;
        this.GetComponent<SphereCollider>().radius = 1.4f;
    }
    public void StartLooking()
    {
        attacking = false;
        navAgent.isStopped = false;
        animator.SetTrigger("walk");
        navAgent.speed = 3f;
        navAgent.acceleration = 8f;
        this.GetComponent<SphereCollider>().radius = 1.4f;
    }

    IEnumerator EnemyAttack()
    {
        //damagePlane.SetActive(true);
        //yield return new WaitForSeconds(0.2f);
        //damagePlane.SetActive(false);
        
        hurtAudio.Play();
        PlayerHealth.healthValue -= damage;
        Debug.Log("PLAYER!");
        yield return new WaitForSeconds(attackSpeed);
    }
}

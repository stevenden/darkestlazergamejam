﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class firstPersonScript : MonoBehaviour
{
    private GameObject cameraFPS;
    public float mouseSensitive;
    float xRotate = 0f;
    public float lowerEnemyHealth;
    public GameObject gun;

    // Start is called before the first frame update
    void Start()
    {
        cameraFPS = this.GetComponentInChildren<Camera>().gameObject;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitive * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitive * Time.deltaTime;

        xRotate -= mouseY;
        xRotate = Mathf.Clamp(xRotate, -90f, 90f);

        cameraFPS.transform.localRotation = Quaternion.Euler(xRotate, 0f, 0f);
        this.transform.Rotate(Vector3.up * mouseX);


        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(gun.transform.position, gun.transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
        {
            //Debug.DrawRay(gun.transform.position, gun.transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            Debug.Log(hit.collider);
            if(hit.collider.gameObject.layer == 10)
            {
                if(hit.collider.gameObject.GetComponent<LowerHealth>() != null)
                {
                    hit.collider.gameObject.GetComponent<LowerHealth>().enemyAI.enemyHealth -= lowerEnemyHealth * Time.deltaTime;
                }
            }
        }

    }
}

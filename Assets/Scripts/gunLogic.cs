﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gunLogic : MonoBehaviour
{
    public ParticleSystem particles;
    public GameObject lightParent;
    public Light[] lazerLights;
    public LineRenderer lineRend;
    public GameObject laserStartPoint;
    

    public float timeLightsOn;
    bool lightsOn;
    private IEnumerator lazerOn;
    public float chargeAvailable, chargeMax, currentCharge, lowerLazerChargeAmt;

    //Will Variables
    public Light batteryLight;
    public float batteryMinIntensity, batteryMaxIntensity, currentIntensity;
    public bool isFiring = false;
    public float targetDistance;
    public GameObject shotSpawn;
    public GameObject bullet;
    public int gunDamage;
    public AudioSource beamFire;
    private bool beamFirePlaying = false;
    public AudioSource bulletFire;


    //float lightTimer;
    // Start is called before the first frame update
    void Start()
    {
        currentCharge = chargeMax;
        currentIntensity = batteryMaxIntensity;
        lineRend.enabled = false;
        var numberoflights = 0;
        foreach(Transform child in lightParent.transform)
        {
            numberoflights += 1;
        }
        lazerLights = new Light[numberoflights];
        for(int x=0; x<numberoflights; x++) { 
            lazerLights[x] = lightParent.transform.GetChild(x).gameObject.GetComponent<Light>();
        }

        foreach (Light l in lazerLights)
        {
            l.enabled = false;
        }
    }

    IEnumerator LazerTimerTillOff()
    {
        yield return new WaitForSeconds(5f);
        //laser off

    }
    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetMouseButton(0))
        {
            //Added if statements to seperate different firing modes and their different behaviors
            if (this.gameObject.CompareTag("Beam"))
            {
                currentCharge -= lowerLazerChargeAmt * Time.deltaTime;

                currentIntensity -= lowerLazerChargeAmt * Time.deltaTime;
                batteryLight.intensity = currentIntensity * 20f;

                if (!beamFirePlaying)
                {
                    beamFire.Play();
                    beamFirePlaying = true;
                }

                Debug.DrawRay(shotSpawn.transform.position, transform.TransformDirection(Vector3.forward) * 20, Color.green);
                RaycastHit theShot;
                if (Physics.Raycast(shotSpawn.transform.position, transform.TransformDirection(Vector3.forward), out theShot))
                {
                    targetDistance = theShot.distance;
                    theShot.transform.SendMessage("DamageEnemy", gunDamage, SendMessageOptions.DontRequireReceiver);
                }

                if (currentCharge <= 0)
                {
                    lineRend.enabled = false;
                    lightsOn = false;
                    particles.Stop();
                    foreach (Light l in lazerLights)
                    {
                        l.enabled = false;
                    }
                    currentCharge = 0f;
                    beamFire.Stop();
                    beamFirePlaying = false;
                }
            }
            else if (this.gameObject.CompareTag("SingleShot"))
            {
                if (isFiring == false)
                {
                    if (currentCharge <= 0)
                    {
                        lineRend.enabled = false;
                        lightsOn = false;
                        particles.Stop();
                        foreach (Light l in lazerLights)
                        {
                            l.enabled = false;
                        }
                        currentCharge = 0f;
                    }
                    else if (currentCharge > 0)
                    {
                        StartCoroutine(SingleShotFiringCo());
                    }
                }
            }
            
            
        }
        if (Input.GetMouseButtonDown(0))
        {
            if (this.gameObject.CompareTag("Beam"))
            {
                Debug.Log("mousehit");
                particles.Play();
                foreach (Light l in lazerLights)
                {
                    l.enabled = true;
                }
                //lightTimer = 0f;
                lightsOn = true;
            }
            
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (this.gameObject.CompareTag("Beam"))
            {
                lineRend.enabled = false;
                lightsOn = false;
                particles.Stop();
                foreach (Light l in lazerLights)
                {
                    l.enabled = false;
                }
                beamFire.Stop();
                beamFirePlaying = false;
            }
        }
        if (!Input.GetMouseButton(0))
        {
            currentCharge += lowerLazerChargeAmt * Time.deltaTime;
            currentIntensity += lowerLazerChargeAmt * Time.deltaTime;
            batteryLight.intensity = currentIntensity * 20f;

            if (currentCharge >= chargeMax)
            {
                currentCharge = chargeMax;
            }
            if (currentIntensity >= batteryMaxIntensity)
            {
                currentIntensity = batteryMaxIntensity;
            }
        }

        if (lightsOn)
        {
            lineRend.enabled = true;
            //Debug.Log(laserStartPoint.transform.position);
            //lineRend.SetPositions(new Vector3[] { laserStartPoint.transform.position, laserStartPoint.transform.position + (laserStartPoint.transform.forward * 100) });
            //lineRend.SetPosition(0, laserStartPoint.transform.position);
            //lineRend.SetPosition(1, laserStartPoint.transform.position + (laserStartPoint.transform.forward * 100));
            //lightTimer += Time.deltaTime;
            /*if(lightTimer >= timeLightsOn)
            {
                lightsOn = false;
                particles.Stop();
                foreach (Light l in lazerLights)
                {
                    l.enabled = false;
                }
            }*/
        }
    }


    IEnumerator SingleShotFiringCo()
    {
        currentCharge -= lowerLazerChargeAmt/2;

        currentIntensity -= lowerLazerChargeAmt/2;
        batteryLight.intensity = currentIntensity * 20f;
        
        if (batteryLight.intensity <= 0)
        {
            batteryLight.intensity = batteryMinIntensity;
        }

        isFiring = true;
        Instantiate(bullet, shotSpawn.transform.position, shotSpawn.transform.rotation);
        //gunFire.Play();
        yield return new WaitForSeconds(0.2f);
        isFiring = false;
    }
}

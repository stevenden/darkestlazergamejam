﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public GameObject healthDisplay;
    public static int healthValue;
    public int internalHealth;
    public GameObject damagePlane;

    private Image image;
    private float imageAlpha;

    // Start is called before the first frame update
    void Start()
    {
        healthValue = 100;
        imageAlpha = 0f;
        image = damagePlane.GetComponent<Image>();
        image.color = new Color(image.color.r, image.color.g, image.color.b, imageAlpha);
    }

    // Update is called once per frame
    void Update()
    {
        if (healthValue <= 0)
        {
            StartCoroutine(LoadAsyncScene());
        }
        if (internalHealth != healthValue)
        {
            SetDamageOpacity();
        }
        internalHealth = healthValue;
        
    }

    void SetDamageOpacity()
    {
        imageAlpha = (100 - healthValue) / 100;
        image.color = new Color(image.color.r, image.color.g, image.color.b, imageAlpha);
        healthDisplay.GetComponent<Text>().text = "Health: " + healthValue;
    }


    IEnumerator LoadAsyncScene()
    {
        //Loads scene in the background as the current scene runs
        //Particularly good for loading screens
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(0);

        //Wait until the asynchonous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}

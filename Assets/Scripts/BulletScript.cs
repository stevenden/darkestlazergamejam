﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public float speed;
    public int damage;
    public float lifetime;
    public ParticleSystem explosionParticle;

    private Rigidbody rb;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = transform.forward * speed;

        Destroy(gameObject, lifetime);
    }

    /*private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            this.SendMessage("DamageEnemy", damage, SendMessageOptions.DontRequireReceiver);
            Instantiate(explosionParticle, gameObject.transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    } */

    private void OnCollisionEnter(Collision collision)
    {
        this.SendMessage("DamageEnemy", damage, SendMessageOptions.DontRequireReceiver);
        Instantiate(explosionParticle, gameObject.transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}

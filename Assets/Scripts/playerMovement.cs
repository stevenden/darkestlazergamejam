﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour
{
    CharacterController controller;
    public float speed;
    public float gravity = -9.81f;
    public Vector3 velocity;
    public float jumpHeight = 3f;

    public Transform groundCheck;
    public float groundDistance = 0.3f;
    public LayerMask groundMask;
    public bool isGrounded;

    public int generateSteps;
    public AudioSource[] stepSound;

    // Start is called before the first frame update
    void Start()
    {
        controller = this.GetComponentInChildren<CharacterController>(); 
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if(isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;
        controller.Move(move * speed * Time.deltaTime);

        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);
        }
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);

        if (velocity.x > 0.2 || velocity.z > 0.2)
        {
            generateSteps = Random.Range(0, 4);
            stepSound[generateSteps].Play();
        }

    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        
        if (hit.gameObject.layer == 10)
        {
            Debug.Log("enem");
            if (hit.gameObject.GetComponent<enemyAI>() != null)
            {
                hit.gameObject.GetComponent<enemyAI>().player = this.gameObject;
            }
        }
    }
    }

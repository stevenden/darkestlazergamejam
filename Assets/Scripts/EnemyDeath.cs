﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyDeath : MonoBehaviour
{
    public float enemyHealth;
    public bool enemyDead = false;
    public GameObject enemyCode;
    public GameObject theEnemy;


    public void DamageEnemy(float damage)
    {
        enemyHealth -= damage;
    }


    // Update is called once per frame
    void Update()
    {
        if (enemyHealth <= 0 && !enemyDead)
        {
            enemyDead = true;
            //theEnemy.GetComponent<Animator>().Play("Death");
            this.GetComponent<enemyAI>().enabled = false;
            this.GetComponent<NavMeshAgent>().enabled = false;
            //enemyCode.SetActive(false);
        }
    }
}

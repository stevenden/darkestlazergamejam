﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StartRoom : MonoBehaviour
{
    public GameObject[] enemies;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 9) {
            foreach (GameObject enemy in enemies)
            {
                //enemy.GetComponent<NavMeshAgent>().destination = other.gameObject.transform.position;
                enemy.GetComponent<enemyAI>().player = other.gameObject;
            }
        }
    }
}

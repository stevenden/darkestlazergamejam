﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelAnimation : MonoBehaviour
{
    public enemyAI enemyai;
    public LargeEnemyAI largeEnemyAi;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void AttackPlayer()
    {
        enemyai.Lunge();
    }
    public void Rest()
    {
        enemyai.Rest();
    }
    public void StartLooking()
    {
        enemyai.StartLooking();
    }
    public void PlayFootSound()
    {
        this.GetComponent<AudioSource>().Play();
    }
}
